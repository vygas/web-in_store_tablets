
// Gulp.js configuration

// modules (npm install all of these and --save-dev them)
const gulp = require('gulp');
const newer = require('gulp-newer');
const htmlclean = require('gulp-htmlclean');
//const stripdebug = require('gulp-strip-debug');
//const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
//const nunjucks = require('gulp-nunjucks-html');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();

const reload = browserSync.reload;

// folders
var folder = {
    src: 'src/',
    build: 'dist/'
};

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass:dev', function() {

    return gulp.src(folder.src+"scss/**/*.scss")
    .pipe(sass({errLogToConsole: true}))
    .pipe(gulp.dest(folder.src+".tmp/css"))
    .pipe(browserSync.stream());
});

gulp.task('serve', ['sass:dev'], function() {
    
    browserSync.init({
        server: {
            baseDir: "src/",
            index: "index.html",
            routes: {
                '/css' : 'src/.tmp/css'
            }
        }
    });
    
    gulp.watch(folder.src+"scss/**/*.scss", ['sass:dev']);
    
    gulp.watch( [folder.src+"html/**/*.html",
                folder.src+"scss/**/*.scss"]
            ).on('change', reload);
});


// image processing
gulp.task('images', function() {
    return gulp.src(folder.src + 'images/**/*')
        .pipe(newer(folder.build + 'images'))
        .pipe(gulp.dest(folder.build + 'images'))
});

// HTML processing
gulp.task('html', function() {
    return gulp.src(folder.src + '**/*.html')
        .pipe(newer(folder.build))
        .pipe(htmlclean())
        .pipe(gulp.dest(folder.build))
});

// image processing
gulp.task('js', function() {
    return gulp.src(folder.src + 'js/**/*.js')
        .pipe(newer(folder.build + 'js'))
        .pipe(gulp.dest(folder.build + 'js'))
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    var postCssOpts = [
       autoprefixer({ browsers: ['last 2 versions', '> 2%'] })
       ];

    return gulp.src(folder.src+"scss/**/*.scss")
        .pipe(sass())
        .pipe(postcss(postCssOpts))
        .pipe(gulp.dest(folder.build+"css"))
});

gulp.task('fonts', function() {
    return gulp.src(folder.src+'fonts/**/*.{eot,svg,ttf,woff,woff2}')
        .pipe(newer(folder.buil + 'fonts'))
        .pipe(gulp.dest(folder.build + 'fonts'))
});


gulp.task('serve:dist', ['html','images','sass', 'js', 'fonts'], function() {

    browserSync.init({
        server: {
            baseDir: folder.build,
            index: "index.html"
        }
    });
});


gulp.task('default', ['serve']);